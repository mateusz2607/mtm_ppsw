#include "led.h"
#include "keyboard.h"

int main(){
  
  enum LedsState{STATE0, STATE1, STATE2, STATE3, STATE4, STATE5};
  enum LedsState eLedsState = STATE0;
  
  LedInit();
  while(1){
    switch(eLedsState){
      case STATE0:
        LedStepLeft();
				eLedsState = STATE1;
        break;
      case STATE1:
        LedStepLeft();
				eLedsState = STATE2;
        break;
      case STATE2:
        LedStepLeft();
				eLedsState = STATE3;
        break;
      case STATE3:
        LedStepRight();
				eLedsState = STATE4;
        break;
      case STATE4:
        LedStepRight();
				eLedsState = STATE5;
        break;
      case STATE5:
        LedStepRight();
				eLedsState = STATE0;
        break;
    }
    delay(50);
  }
}
