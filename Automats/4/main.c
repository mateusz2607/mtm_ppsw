#include "led.h"
#include "keyboard.h"

int main(){
  
  enum PointMoveDirection{STAY, MOVE_RIGHT};
  enum PointMoveDirection ePointMoveDirection = MOVE_RIGHT;
  char cStepsCounter = 0;
  
  LedInit();
  KeyboardInit();
  while(1){
    switch(ePointMoveDirection){
      case STAY:
        if (eKeyboardRead() == BUTTON_1){
            ePointMoveDirection = MOVE_RIGHT;
        }
        break;
      case MOVE_RIGHT:
        LedStepRight();
        cStepsCounter ++;
        if (cStepsCounter > 3){
            ePointMoveDirection = STAY;
            cStepsCounter = 0;
        }
        break;
    }
    delay(50);
  }
}

