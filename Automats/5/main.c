#include "led.h"
#include "keyboard.h"

int main(){
  
  enum PointMoveDirection{STAY, MOVE_RIGHT};
  enum PointMoveDirection ePointMoveDirection = MOVE_RIGHT;
  
  LedInit();
  KeyboardInit();
  while(1){
    switch(ePointMoveDirection){
      case STAY:
        if (eKeyboardRead() == BUTTON_1){
          ePointMoveDirection = MOVE_RIGHT;
        }
        break;
      case MOVE_RIGHT:
        if (eKeyboardRead() == BUTTON_4){
          ePointMoveDirection = STAY;
        }
        else{
          LedStepRight();   
        }
        break;
    }
    delay(50);
  }
}
