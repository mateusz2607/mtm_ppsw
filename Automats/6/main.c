#include "led.h"
#include "keyboard.h"

int main(){
  
  enum PointMoveDirection{STAY, MOVE_RIGHT, MOVE_LEFT};
  enum PointMoveDirection ePointMoveDirection = STAY;
  
  LedInit();
  KeyboardInit();
  while(1){
    switch(ePointMoveDirection){
      case STAY:
        if (eKeyboardRead() == BUTTON_1){
          ePointMoveDirection = MOVE_LEFT;
        }
        else if (eKeyboardRead() == BUTTON_3){
          ePointMoveDirection = MOVE_RIGHT;
        }
        break;
      case MOVE_RIGHT:
        if (eKeyboardRead() == BUTTON_2){
          ePointMoveDirection = STAY;
        }
        else{
          LedStepRight(); 
        }
        break;
      case MOVE_LEFT:
        if (eKeyboardRead() == BUTTON_2){
          ePointMoveDirection = STAY;
        }
        else{
          LedStepLeft(); 
        }
        break;
    }
    delay(100);
  }
}
