#define RECIEVER_SIZE  4
#define TRANSMITER_SIZE 15

enum eRecieverStatus {EMPTY, READY, OVERFLOW};

struct RecieverBuffer{
  char cData[RECIEVER_SIZE];
  unsigned char ucCharCtr;
  enum eRecieverStatus eStatus;
};

__irq void UART0_Interrupt (void);
void UART_InitWithInt(unsigned int uiBaudRate);
void Reciever_PutCharacterToBuffer(char cCharacter);
enum eRecieverStatus eReciever_GetStatus(void);
void Reciever_GetStringCopy(char * ucDestination);
