#include "uart.h"
#include "string.h"
#include "servo.h"


int main (){
  
  char cTablica[RECIEVER_SIZE];
  
  UART_InitWithInt(115200);
  ServoInit(50);
  
	while(1){
    
    if(eReciever_GetStatus() == READY){
      Reciever_GetStringCopy(cTablica);
      if(eCompareString(cTablica,"callib") == EQUAL){
      ServoCallib();
      }
      else if(eCompareString(cTablica,"left") == EQUAL){
        ServoGoTo(12);
      }
      else if(eCompareString(cTablica,"right") == EQUAL){
        ServoGoTo(36);
      }
    }
	}
}
