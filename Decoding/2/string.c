#include "string.h"


void CopyString(char pcSource[], char pcDestination[]){
  
	unsigned char ucCharCounter;
  
	for(ucCharCounter=0;pcSource[ucCharCounter]!='\0';ucCharCounter++){
		pcDestination[ucCharCounter] = pcSource[ucCharCounter];
	}
	pcDestination[ucCharCounter] = '\0';
}

enum CompResult eCompareString(char pcStr1[], char pcStr2[]){
  
	unsigned char ucCharCounter;
  
	for(ucCharCounter=0;pcStr1[ucCharCounter] != '\0';ucCharCounter++){
		if (pcStr1[ucCharCounter] != pcStr2[ucCharCounter]){
      return DIFFERENT;
    }
	}
	return EQUAL;
}

