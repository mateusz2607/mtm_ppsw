#define NULL 0
#define TERMINATOR '\r'
#define DELIMITER_CHAR 0x20

enum CompResult{DIFFERENT,EQUAL};
enum Result{OK,ERROR};

void CopyString(char pcSource[], char pcDestination[]);
enum CompResult eCompareString(char pcStr1[], char pcStr2[]);

