#include <LPC210X.H>
#include "uart.h"
#include "string.h"

#define mP0_1_RX0_PIN_MODE 							           0x00000004  // P0.1 jako RxD UART0 
#define mP0_0_TX0_PIN_MODE 							           0x00000001  // P0.0 jako TxD UART0

#define mDIVISOR_LATCH_ACCES_BIT                   0x00000080  // zezwala na modyfikację prędkości transmisji
#define m8BIT_UART_WORD_LENGTH                     0x00000003  // 0b11 na bitach WordLenghtSelect powoduje ustawienie długości ramki danych na 8 bitów

#define mRX_DATA_AVALIABLE_INTERRUPT_ENABLE        0x00000001
#define mTHRE_INTERRUPT_ENABLE                     0x00000002

#define mINTERRUPT_PENDING_IDETIFICATION_BITFIELD  0x0000000F
#define mTHRE_INTERRUPT_PENDING                    0x00000002
#define mRX_DATA_AVALIABLE_INTERRUPT_PENDING       0x00000004


#define VIC_UART0_CHANNEL_NR  6 // UART0 jako źródło przerwania
#define VIC_UART1_CHANNEL_NR  7 // UART1 jako źródło przerwania

#define mIRQ_SLOT_ENABLE                           0x00000020 // obdługa przerwania


char cOdebranyZnak;
char cFlaga;


struct RecieverBuffer sRxBuffer;


void Reciever_PutCharacterToBuffer(char cCharacter){
  
  if(sRxBuffer.ucCharCtr >= RECIEVER_SIZE){
    sRxBuffer.eStatus = OVERFLOW;
  }
  else if(cCharacter == TERMINATOR){
    sRxBuffer.cData[sRxBuffer.ucCharCtr] = NULL;
    sRxBuffer.eStatus = READY;
    sRxBuffer.ucCharCtr = 0;
  }
  else {
    sRxBuffer.cData[sRxBuffer.ucCharCtr] = cCharacter;
    sRxBuffer.ucCharCtr ++;
  }
}


enum eRecieverStatus eReciever_GetStatus(void){
  
  return sRxBuffer.eStatus;
}


void Reciever_GetStringCopy(char *ucDestination){
  
  CopyString(sRxBuffer.cData,ucDestination);
  sRxBuffer.eStatus = EMPTY;
}


__irq void UART0_Interrupt (void) {
  
  unsigned int uiCopyOfU0IIR=U0IIR;

  if ((uiCopyOfU0IIR & mINTERRUPT_PENDING_IDETIFICATION_BITFIELD) == mRX_DATA_AVALIABLE_INTERRUPT_PENDING){
    cFlaga = 0x1;
    cOdebranyZnak = U0RBR;
    Reciever_PutCharacterToBuffer(cOdebranyZnak);
  } 
   
  if ((uiCopyOfU0IIR & mINTERRUPT_PENDING_IDETIFICATION_BITFIELD) == mTHRE_INTERRUPT_PENDING){

  }
  VICVectAddr = 0;
}


void UART_InitWithInt(unsigned int uiBaudRate){


   PINSEL0 = PINSEL0 | mP0_1_RX0_PIN_MODE | mP0_0_TX0_PIN_MODE;   // ustawiamy piny jako RxD i TxD
   U0LCR  |= m8BIT_UART_WORD_LENGTH | mDIVISOR_LATCH_ACCES_BIT;   // ustawiamy długość słowa i zezwalamy na modyfikację baudrate
   U0DLL   = (((15000000)/16)/uiBaudRate); // VPBDIV musi 15MHz podzielić tak żeby dostać 16* baudrate  
   U0LCR  &= (~mDIVISOR_LATCH_ACCES_BIT); // blukujemy możlowość zmiany baudrate
   U0IER  |= mRX_DATA_AVALIABLE_INTERRUPT_ENABLE; // włączamy przerwania po otrzymaniu danych


   VICVectAddr1  = (unsigned long) UART0_Interrupt; // podpinamy procedurę obsługi przerwania
   VICVectCntl1  = mIRQ_SLOT_ENABLE | VIC_UART0_CHANNEL_NR; // wybór źródła i procedury obsługi przerwania
   VICIntEnable |= (0x1 << VIC_UART0_CHANNEL_NR); // włączamy przterwania z uarta0 
}
