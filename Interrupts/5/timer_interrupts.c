#include <LPC21xx.H>
#include "timer_interrupts.h"


#define COUNTER_ENABLE_bm 1
#define COUNTER_RESET_bm (1<<1)

#define INTERRUPT_ON_MR0_bm 1
#define RESET_ON_MR0_bm (1<<1)
#define MR0_INTERRUPT_bm 1

#define VIC_TIMER0_CHANNEL_NR 4

#define IRQ_SLOT_ENABLE_bm (1<<5)

#define US_TO_CLK 15


void (*ptrTimer0InterruptFunction)(void);


__irq void Timer0IRQHandler(){

	T0IR=MR0_INTERRUPT_bm; 	
	ptrTimer0InterruptFunction();	
	VICVectAddr=0x00; 	
}


void Timer0Interrupts_Init(unsigned int uiPeriod, void (*ptrInterruptFunction)(void)){ 

	VICIntEnable |= (0x1 << VIC_TIMER0_CHANNEL_NR);            
	VICVectCntl0  = IRQ_SLOT_ENABLE_bm | VIC_TIMER0_CHANNEL_NR;  
	VICVectAddr0  =(unsigned long)Timer0IRQHandler; 	  
	T0MR0 = US_TO_CLK * uiPeriod;                 	   	
	T0MCR |= (INTERRUPT_ON_MR0_bm | RESET_ON_MR0_bm); 
	T0TCR |=  COUNTER_ENABLE_bm; 
  ptrTimer0InterruptFunction = ptrInterruptFunction;
}
