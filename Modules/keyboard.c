#include <LPC21xx.H>
#include "keyboard.h"


#define BUT1_bm (1<<4)
#define BUT2_bm (1<<6)
#define BUT3_bm (1<<5)
#define BUT4_bm (1<<7)


void KeyboardInit(void){

  IO0DIR=IO0DIR&(~(BUT1_bm|BUT2_bm|BUT3_bm|BUT4_bm));
}


int eKeyboardRead(void){
  
  enum KeyboardState eKeyboardStateToReturn=RELASED;
  
  if (~IO0PIN & BUT1_bm){
    eKeyboardStateToReturn=BUTTON_1;
  }
  else if (~IO0PIN & BUT2_bm){
    eKeyboardStateToReturn=BUTTON_2;
  }
  else if (~IO0PIN & BUT3_bm){
    eKeyboardStateToReturn=BUTTON_3;
  }
  else if (~IO0PIN & BUT4_bm){
    eKeyboardStateToReturn=BUTTON_4;
  }

  return eKeyboardStateToReturn;
} 
