#include <LPC21xx.H>
#include "led.h"
#include "keyboard.h"
#include "timer_interrupts.h"
#include "servo.h"


void Automat(){
  
  enum State{CALLIB, STAY, MOVE_RIGHT, MOVE_LEFT};
  static enum State eState = CALLIB;

  switch(eState){
    case CALLIB:
			if(eReadDetector() == INACTIVE)
				LedStepRight();
			else
				eState = STAY;
			break;
    case STAY:
      if (eKeyboardRead() == BUTTON_1){
        eState = MOVE_LEFT;
      }
      else if (eKeyboardRead() == BUTTON_3){
        eState = MOVE_RIGHT;
      }
      break;
    case MOVE_RIGHT:
      if (eKeyboardRead() == BUTTON_2){
        eState = STAY;
      }
      else{
        LedStepRight(); 
      }
      break;
    case MOVE_LEFT:
      if (eKeyboardRead() == BUTTON_2){
        eState = STAY;
      }
      else{
        LedStepLeft(); 
      }
      break;
  }
}


int main (){
	
	unsigned int iMainLoopCtr;
  
  DetectorInit();
	Timer0Interrupts_Init(200000, &Automat);
	while(1){
	 	iMainLoopCtr++;
	}
}
