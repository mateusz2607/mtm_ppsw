#include "timer.h"
#include "led.h"
#include "keyboard.h"
#include "timer_interrupts.h"
#include "servo.h"
#include "uart.h"


int main (){
  
	extern char cOdebranyZnak;
	unsigned char ucZnak;
	
	KeyboardInit();
	ServoInit(50);
	LedInit();
	UART_InitWithInt(115200);

	while(1){
		if (ucZnak != cOdebranyZnak){
			switch (cOdebranyZnak){
				case ('1'):
					ServoGoTo(50);
					break;
				case ('2'):
					ServoGoTo(100);
					break;
				case ('3'):
					ServoGoTo(150);
					break;
				case ('C'):
					ServoCallib();
					break;
			}
			ucZnak = cOdebranyZnak;
		}

		if(eKeyboardRead() == BUTTON_2)
			ServoGoTo(12);
		if(eKeyboardRead() == BUTTON_3)
			ServoGoTo(24);
		if(eKeyboardRead() == BUTTON_4)
			ServoGoTo(48);
	}
}
