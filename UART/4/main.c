#include "timer.h"
#include "led.h"
#include "keyboard.h"
#include "timer_interrupts.h"
#include "servo.h"
#include "uart.h"


int main (){
  
	extern char cOdebranyZnak;
  extern char cFlaga;
	unsigned char ucPosition;

	KeyboardInit();
	LedInit();
	UART_InitWithInt(115200);
  ServoInit(50);
	while(1){
		if (cFlaga != 0){
			switch (cOdebranyZnak){
				case ('1'):
					ucPosition = 24 + ucPosition;
					break;
				case ('C'):
          ucPosition = 0;
					ServoCallib();
					break;
				default:
					break;
			}
			ServoGoTo(ucPosition);
      cFlaga = 0;
		}
	}
}
